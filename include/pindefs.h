/**
 * Contains definition macros such as pin numbers for a variety of
 * components (ex. motors, encoders) and dimensions.
*/

#pragma once

// Motor pin numbers
#define BACK_LEFT_WHEELS_PORT 11
#define BACK_RIGHT_WHEELS_PORT 20
#define FRONT_LEFT_WHEELS_PORT 12
#define FRONT_RIGHT_WHEELS_PORT 19
#define LIFT_RIGHT_PORT 10
#define LIFT_LEFT_PORT 1

// Encoder pin numbers

// Robot dimensions in inches (TODO: Update to real dimensions)
#define DRIVE_WHEEL_DIAMETER 3.25f
#define TRACKING_WHEEL_DIAMETER 2.75f
#define WHEELBASE 10.25f
#define BACK_WHEEL_OFFSET 5.0f

// Sensor ports

// Digital Out Ports
#define PISTON_PORT 'A'