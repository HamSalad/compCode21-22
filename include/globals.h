/**
 * Contains global variables such as motors and encoders
 * that are used throughout the project.
 */

#pragma once

#include "main.h"

// Motors
extern pros::Motor back_left_wheels;
extern pros::Motor back_right_wheels;
extern pros::Motor front_left_wheels;
extern pros::Motor front_right_wheels;
extern pros::Motor lift_right;
extern pros::Motor lift_left;
extern pros::ADIDigitalOut piston;


// Encoders

// Actual controller
extern pros::Controller master;

// Sensors
