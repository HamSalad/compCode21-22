#include "main.h"
#include "globals.h"

/**
 * Motor Definitons
 */

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
  pros::lcd::initialize();
  pros::lcd::set_text(1, "WIRELESS TEST");
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {}

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {}

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */

void opcontrol() {

  // Deadband for joystick to remove crawl effect
  int deadband = 7;

  motor_setup();



  while (true) {
    // Map left and right side speeds to left and right joystick y-axis
    int driveTrainSpeedL = master.get_analog(ANALOG_LEFT_Y);
    int driveTrainSpeedR = master.get_analog(ANALOG_RIGHT_Y);

    // Ensures joystick is moved beyond deadband to remove crawl effect
    if (abs(driveTrainSpeedL) > deadband || abs(driveTrainSpeedR) > deadband) {
      back_left_wheels.move(driveTrainSpeedL);
      back_right_wheels.move(driveTrainSpeedR);
      front_left_wheels.move(driveTrainSpeedL);
      front_right_wheels.move(driveTrainSpeedR);
    }
    // If not beyond deadband, sets speed to 0
    else {
      back_left_wheels.move(0);
      back_right_wheels.move(0);
      front_left_wheels.move(0);
      front_right_wheels.move(0);
    }

    if (master.get_digital(DIGITAL_R1)) {
      lift_left.move(75);
      lift_right.move(75);
    } else {
      if (master.get_digital(DIGITAL_R2)) {
        lift_left.move(-75);
        lift_right.move(-75);
      } else {
        lift_left.move(0);
        lift_right.move(0);
      }
    }
    /*
    bool currState;
    bool prevState;
    bool solenoid;

        if (master.get_digital(DIGITAL_UP)) {
          pros::delay(10);
          if (master.get_digital(DIGITAL_UP)) {
            currState = true;
          }
        } else {
          pros::delay(10);
          if (master.get_digital(DIGITAL_UP)) {
            currState = false;
          }
        }
        if (currState != prevState) {
          if (currState) {
            solenoid = !solenoid;
          }
        }
        prevState = currState;

        if (solenoid) {
          piston.set_value(true);
        } else {
          piston.set_value(false);
        }
*/
    bool toggle;
    if (master.get_digital_new_press(DIGITAL_UP)) {
      toggle = !toggle;
      piston.set_value(toggle);
    }

    /*
        bool reliable;
        currState = master.get_digital(DIGITAL_UP);
        reliable = currState == prevState;
        prevState = currState;

        if (!reliable) {
          // btn_pressed is not yet reliable, treat as not pressed
          currState = false;
        }

        // <-- here btn_pressed contains the state of the switch, do something
    */
    


    pros::delay(10);
  }
}
