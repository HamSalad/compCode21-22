#include "globals.h"
#include "main.h"
#include "pindefs.h"

pros::Motor back_left_wheels(BACK_LEFT_WHEELS_PORT);
pros::Motor back_right_wheels(BACK_RIGHT_WHEELS_PORT, true);
pros::Motor front_left_wheels(FRONT_LEFT_WHEELS_PORT);
pros::Motor front_right_wheels(FRONT_RIGHT_WHEELS_PORT, true);
pros::Motor lift_right(LIFT_RIGHT_PORT);
pros::Motor lift_left(LIFT_LEFT_PORT, true);
pros::ADIDigitalOut piston(PISTON_PORT);

pros::Controller master(CONTROLLER_MASTER);

int motor_setup(void) {
  back_left_wheels.set_brake_mode(MOTOR_BRAKE_COAST);
  back_right_wheels.set_brake_mode(MOTOR_BRAKE_COAST);
  front_left_wheels.set_brake_mode(MOTOR_BRAKE_COAST);
  front_right_wheels.set_brake_mode(MOTOR_BRAKE_COAST);
  lift_left.set_brake_mode(MOTOR_BRAKE_HOLD);
  lift_right.set_brake_mode(MOTOR_BRAKE_HOLD);

  back_left_wheels.set_gearing(MOTOR_GEARSET_18);
  back_right_wheels.set_gearing(MOTOR_GEARSET_18);
  front_left_wheels.set_gearing(MOTOR_GEARSET_18);
  front_right_wheels.set_gearing(MOTOR_GEARSET_18);
  lift_left.set_gearing(MOTOR_GEARSET_36);
  lift_right.set_gearing(MOTOR_GEARSET_36);
  return 0;
}