# 241H VEX Robotics Competition Code

This is the competition code for Vex EDR team 241H. It includes BOTH autonomous and opcontrol code, and is written using the competition template.

## Getting Started

To modify or compile this code on your own machine, begin by installing VEX PROS. [PROS Download](https://pros.cs.purdue.edu/v5/getting-started/) PROS offers a cli utility, or plugnins for Atom, or Visual Studio.

Next, create a PROS project, and clone this repository into the same folder.
